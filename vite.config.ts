import { defineConfig } from 'vite'
import preact from '@preact/preset-vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [preact()],
  build: {
    modulePreload: {
      resolveDependencies: (url, deps, context) => {
        return [];
      },
    },
    rollupOptions: {
      output: {
        sourcemap: false,
        manualChunks: {
          'preact-router': ['preact-router'],
          'preact-async-route': ['preact-async-route'],
        },
      },
    },
  },
  server: {
    port: 3000,
  },
})
