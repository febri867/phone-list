// import { useState } from 'preact/hooks'
import Router from 'preact-router';
import './app.scss';
import AsyncRoute from 'preact-async-route';
export function App() {
  // const [count, setCount] = useState(0)

  return (
    <Router>
      <AsyncRoute
        path={'/sap_mobility/mobile-devices'}
        getComponent={() => import('./pages/mobile-devices').then((module) => module.default)}
      />
    </Router>
  );
}
