import Header from './components/header';
import Introduction from './components/introduction';
import List from './components/list';

const MobileDevices = () => {
  return (
    <>
      <Header />
      <Introduction />
      <List />
    </>
  );
};

export default MobileDevices;
