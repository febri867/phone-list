import { useState } from 'preact/hooks';
import './index.scss';

const Header = () => {
  const [active, setActive] = useState('products');
  const handleOnClick = (e: any) => {
    setActive(e.target.name);
  };
  return (
    <>
      <ul id="nav" class="nav nav-tabs">
        <img
          class={'logo'}
          src={'https://ontego.de/images/commsult/logos/Ontego_Business_Mobi.svg'}
          alt={'ontego'}
        />
        <li class="nav-item">
          <a
            onClick={handleOnClick}
            name={'products'}
            class={`nav-link ${active === 'products' && 'active'}`}
            aria-current="page"
            href="#"
          >
            Products
          </a>
        </li>
        <li class="nav-item">
          <a
            name={'why'}
            onClick={handleOnClick}
            class={`nav-link ${active === 'why' && 'active'}`}
            href="#"
          >
            Why Ontego?
          </a>
        </li>
        <li class="nav-item">
          <a
            name={'events'}
            onClick={handleOnClick}
            class={`nav-link ${active === 'events' && 'active'}`}
            href="#"
          >
            Events
          </a>
        </li>
        <li class="nav-item">
          <a
            name={'resources'}
            onClick={handleOnClick}
            class={`nav-link ${active === 'resources' && 'active'}`}
            aria-disabled="true"
          >
            resources
          </a>
        </li>
        <button type="button" className="btn btn-req-demo">
          Request a demo <img class={'arrow'} alt={'arrow'} src={'/arrow.png'} />
        </button>
      </ul>
      <div id={'nav-content'}>
        <div class={'container-subtitle'}>
          <img
            src="https://ontego.de/images/commsult/logos/Ontego_subline.svg"
            alt="Ontego SAP Mobility"
          />
          <p class={'submenu-title'}>Features</p>
          <p class={'submenu-title'}>Areas of application</p>
          <p class={'submenu-title'}>mobile devices</p>
        </div>
      </div>
    </>
  );
};

export default Header;
