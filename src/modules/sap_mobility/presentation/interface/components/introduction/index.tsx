import './index.scss';
const Introduction = () => {
  return (
    <div class={'container-introduction'}>
      <div className={'row'}>
        <div className={'offset-sm-4 col-sm-8'}>
          <img
            class={'img-introduction'}
            src={'https://ontego.de/images/commsult/content/A.1.3/A.1.3_01_M1_Intro@2x.jpg'}
            alt={'bg-img'}
          />
        </div>
        <div className={'col-md-6'} style={'z-index: 1'}>
          <h1 class={'title'}>How do I find the best mobile devices for my employees?</h1>
        </div>
        <div className="col-lg-8 offset-lg-2">
          <p className="cs-body-1">
            Mobile applications are now part of everyday life in companies: in production, warehouse
            logistics, maintenance or delivery. We give you an overview of mobile hardware from
            handheld computers to mobile scanners, forklift terminals and tablets to robust
            smartphones and mobile printers.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Introduction;
