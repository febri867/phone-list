import './index.scss';
import Isotope from 'isotope-layout';
import { useEffect } from 'preact/hooks';
import ExampleData from '../../../../../../assets/example.json';

const List = () => {
  // const [isChecked, setIsChecked] = useState('*')

  let iso: any;
  useEffect(() => {
    iso = new Isotope('.grid', {
      itemSelector: '.element-item',
      layoutMode: 'fitRows',
      getSortData: {
        name: '.name',
      },
    });
  });

  const radioButtonGroup = (buttonGroup: any) => {
    buttonGroup.addEventListener('click', function (event: any) {
      buttonGroup.querySelector('.is-checked').classList.remove('is-checked');
      event.target.classList.add('is-checked');
    });
  };

  const changeButtonChecked = () => {
    let buttonGroups = document.querySelectorAll('.button-group');
    for (let i = 0, len = buttonGroups.length; i < len; i++) {
      let buttonGroup = buttonGroups[i];
      radioButtonGroup(buttonGroup);
    }
  };

  const handleFilterClick = (e: any) => {
    e.preventDefault();
    let filterValue = e.target.getAttribute('data-filter');
    iso.arrange({ filter: filterValue });
    changeButtonChecked();
  };

  const handleSorterClick = (e: any) => {
    e.preventDefault();
    let sortByValue = e.target.getAttribute('data-sort-by');
    iso.arrange({ sortBy: sortByValue });
    changeButtonChecked();
  };

  return (
    <>
      <div class="filter-container">
        <div class="row">
          <div class="col-1">
            <b>Filter</b>
          </div>
          <div class="col-11">
            <div id="filters" className="button-group">
              <button onClick={handleFilterClick} className={`button is-checked`} data-filter="*">
                show all
              </button>
              <button onClick={handleFilterClick} className={`button`} data-filter=".in-stores-now">
                In Stores Now
              </button>
              <button
                onClick={handleFilterClick}
                className={`button`}
                data-filter=".latest-devices"
              >
                Latest Devices
              </button>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-1">
            <b>Sort</b>
          </div>
          <div class="col-11">
            <div id="sorts" className="button-group">
              <button
                onClick={handleSorterClick}
                className="button is-checked"
                data-sort-by="original-order"
              >
                original order
              </button>
              <button onClick={handleSorterClick} className="button" data-sort-by="name">
                name
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div className="grid row">
          {ExampleData.map((e) => (
            <div className="col-md-3">
              <div class={'element-item ' + e.category} data-category={e.category}>
                <div class="container-img">
                  <img src={e.device_image} alt={e.key} />
                </div>
                <p className="name">{e.device_name}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default List;
