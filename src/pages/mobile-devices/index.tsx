import MobileDevices from '../../modules/sap_mobility/presentation/interface/mobile-devices.tsx';

const MobileDevicePage = () => {
  return (
    <>
      <MobileDevices />
    </>
  );
};

export default MobileDevicePage;
